const createError = require("http-errors");
const express = require("express");
const path = require("path");
const helmet = require("helmet");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const sassMiddleware = require('node-sass-middleware');
const mongoose = require("mongoose");
const session = require("express-session");
const MemoryStore = require("memorystore")(session);
const passport = require("passport");
const debug = require("debug")("simple-blog:http");

const {config} = require("./config/config");

const indexRouter = require("./routes/index");
const postsRouter = require("./routes/posts");
const authorsRouter = require("./routes/authors");


// Check environment variables
const mongodbURI = process.env.MONGODB_URI;
const sessionSecret = process.env.SESSION_SECRET;

(() => {
	let exit = false;
	if (!mongodbURI) {
		console.log(
			"A valid MongoDB URI must be provided using the MONGODB_URI\n" +
			"environment variable."
		);
		exit = true;
	}
	if (!sessionSecret) {
		console.log(
			"A string must be provided to be used as the session secret\n" +
			"using the SESSION_SECRET environment variable."
		);
		exit = true;
	}
	if (exit) {
		process.exit(1);
	}
})();


// Set up application instance
const app = express();

app.locals.blogTitle = config.blogTitle;
app.locals.allowRegistration = config.allowRegistration;


// Connect to database
mongoose.connect(mongodbURI, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
})
	.catch((err) => {
		console.log("MongoDB connection error, shutting down: " + err);
		setTimeout(() => process.exit(1), 0);
	});

let db = mongoose.connection;
db.on("error", (err) => {
	console.log("MongoDB error: " + err);
});
db.on("connected", () => console.log("Connected to database!"));
db.on("disconnected", () => console.log("Disconnected from database!"));


// Set up view engine
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

// Middleware
app.use(logger("dev"));
app.use(helmet({
	contentSecurityPolicy: {
		directives: {
			// By default only local scripts will be able to run.
			// Whitelist external script sources manually here.
			scriptSrc: ["'self'"],
		}
	},
	crossdomain: true,
	frameguard: {
		action: "deny",
	},
	referrerPolicy: true,
}));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({
	store: new MemoryStore({
		checkPeriod: 86400000,  // ms
	}),
	cookie: {
		sameSite: "Lax",
	},
	name: "b-sid",
	secret: sessionSecret,
	httpOnly: true,
	resave: false,  // Depends on the store used
	saveUninitialized: false,
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(sassMiddleware({
	src: path.join(__dirname, "public"),
	dest: path.join(__dirname, "public"),
	indentedSyntax: false,
	sourceMap: true
}));
app.use(express.static(path.join(__dirname, "public")));

// Routes
app.use("/", indexRouter);
app.use("/posts", postsRouter);
app.use("/authors", authorsRouter);

// Passport config
let Author = require("./models/author");
passport.use(Author.createStrategy());
passport.serializeUser(Author.serializeUser());
passport.deserializeUser(Author.deserializeUser());


// Catch 404 and forward to error handler
app.use(function(req, res, next) {
	next(createError(404));
});


// Error handler
app.use(function(err, req, res, next) {
	debug(err);

	// Set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};

	// Render the error page
	res.status(err.status || 500);
	res.render("error");
});


module.exports = app;
