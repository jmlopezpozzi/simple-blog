"use strict";


const fs = require("fs");
const debug = require("debug")("simple-blog:config")


const defaultValues = {
	blogTitle: "Blog",
	allowRegistration: false,
	dateFormat: "DD/MM/YYYY",
	postList: {
		postsPerPage: 10,
		showPreview: true,
		previewLength: 512,
		previewMargin: 32,
	},
	authorShow: {
		postsPerPage: 10,
		showPreview: false,
		previewLength: 512,
		previewMargin: 32,
	},
};

const envConfig = process.env.BLOG_CONFIG;
const defaultFile = "config/config.json";


const config = {};


function deepObjectAssign(destination, source) {
	let keys = Object.keys(source);
	for (let key of keys) {
		if (source[key] !== null && typeof(source[key]) === "object") {
			if (destination[key] == undefined) {
				destination[key] = {};
			}
			deepObjectAssign(destination[key], source[key]);
			continue;
		}
		destination[key] = source[key];
	}
}


function deepDestPropsOnlyObjectAssign(destination, source) {
	let keys = Object.keys(destination);
	for (let key of keys) {
		if (source[key] == undefined) {
			continue;
		}
		if (destination[key] !== null && typeof(destination[key]) === "object")
		{
			deepDestPropsOnlyObjectAssign(destination[key], source[key]);
			continue;
		}
		destination[key] = source[key];
	}
}


function deepObjectFreeze(destination) {
	let keys = Object.keys(destination);
	for (let key of keys) {
		if (destination[key] !== null && typeof(destination[key]) === "object")
		{
			deepObjectFreeze(destination[key]);
		}
	}
	Object.freeze(destination);
}


deepObjectFreeze(defaultValues);
deepObjectAssign(config, defaultValues);

try {
	let configFile = fs.readFileSync(defaultFile);
	let parsedObject = JSON.parse(configFile);
	deepDestPropsOnlyObjectAssign(config, parsedObject);
}
catch (err) {
	console.log("Error opening or parsing default configuration file");
	debug(err);
}


function applyExternalConfig(source, destination, sourceName) {
	if (!source) {
		return;
	}
	if (/.\.json$/.test(source)) {
		try {
			let userFile = fs.readFileSync(source);
			let parsedObject = JSON.parse(userFile);
			deepDestPropsOnlyObjectAssign(destination, parsedObject);
		}
		catch (err) {
			console.log(`Error opening or parsing ${sourceName} configuration file`);
			debug(err);
		}
	}
	else {
		try {
			let parsedObject = JSON.parse(source);
			deepDestPropsOnlyObjectAssign(destination, parsedObject);
		}
		catch (err) {
			console.log(`Error parsing ${sourceName} configuration string`);
			debug(err);
		}
	}
}


applyExternalConfig(envConfig, config, "environment");
applyExternalConfig(process.argv[2], config, "user");

deepObjectFreeze(config);


exports.config = config;
