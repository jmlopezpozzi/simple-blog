/* Authors controller */

"use strict";


const {body, param, validationResult} = require("express-validator");

const {config} = require("../config/config");

const Author = require("../models/author");
const Post = require("../models/post");

const {
	getPostPreviewObject,
	fieldCheckIdAndRetrieve,
	assertLoggedUser,
	sortPosts
} = require("./controllerHelpers");

const {
	getPage,
	getAuthorPostsPerPage,
	getAuthorPostListPage,
	getAuthorPostListNumPages,
	deleteAuthorPostList,
	getLoggedAuthorPostListPage,
	getLoggedAuthorPostListNumPages,
	deleteLoggedAuthorPostList,
	generatePostListPages,
} = require("./postPagination");


exports.author_list = (req, res, next) => {
	Author.find()
		.then((authorList) => {
			res.render("author_list", {
				title: "Author list",
				user: req.user,
				authorList,
			});
		})
		.catch((err) => next(err));
}


const authorDataValidateAndSanitize = [
	body("name", "Name must not be empty nor have more than 128 characters")
		.stripLow()
		.trim()
		.isLength({min: 1, max: 128}),
];


exports.author_update = [
	assertLoggedUser,

	(req, res, next) => {
		res.render("author_form", {
			title: "Update Author " + req.user.username,
			user: req.user,
		});
	},
];


exports.author_update_post = [
	assertLoggedUser,
	authorDataValidateAndSanitize,

	(req, res, next) => {
		let author = req.user;
		let authorUpdate = {
			name: req.body.name,
		};
		Object.assign(author, authorUpdate);
		let result = validationResult(req);
		if (!result.isEmpty()) {
			let errors = result.array();
			res.render("author_form", {
				title: "Update Author " + author.username,
				user: author,
				errors,
			});
			return;
		}
		author.save()
			.then((author) => res.redirect(author.url))
			.catch((err) => next(err));
	},
];


const minPreviewCharacters = config.authorShow.previewLength;
const extraPreviewCharacters = config.authorShow.previewMargin;

const getPostPreview =
	config.authorShow.showPreview ?
	(post) => {
		post.preview = getPostPreviewObject(post, minPreviewCharacters,
		                                    extraPreviewCharacters, " ...");
	} :
	(post) => {};


function authorShowGeneric(author, deletePrompt = false) {
	return (req, res, next) => {
		let numPagesFunction = getAuthorPostListNumPages;
		let inPageFunction = getAuthorPostListPage;
		if (req.user && req.user._id.toString() == author._id.toString()) {
			numPagesFunction = getLoggedAuthorPostListNumPages;
			inPageFunction = getLoggedAuthorPostListPage;
		}

		let numPages = numPagesFunction(author);
		let page = getPage(req.query.page, 1, numPages);  // NOTE: Page is 1-indexed
		if (!page) {
			let err = new Error(req.app.get("env") === "development" ?
			                    "Invalid page number" :
			                    "404 - Not found");
			err.status = 404;
			next(err);
			return;
		}

		Post.find({"_id": {$in: inPageFunction(author, page - 1)}})
			.limit(getAuthorPostsPerPage())
			.then((postList) => {
				sortPosts(postList);
				postList.forEach(getPostPreview);
				res.render("author_show", {
					title: "Posts by " + author.name,
					user: req.user,
					author,
					postList,
					deletePrompt,
					page,
					numPages,
				});
			})
			.catch((err) => next(err));
	};
}


exports.author_show = [
	fieldCheckIdAndRetrieve(Author, "author"),

	(req, res, next) => {
		authorShowGeneric(res.locals.author)(req, res, next);
	},
];


exports.author_delete = [
	assertLoggedUser,

	(req, res, next) => {
		authorShowGeneric(req.user, true)(req, res, next);
	},
]


exports.author_delete_post = [
	assertLoggedUser,

	(req, res, next) => {
		Post.deleteMany({author: req.user})
			.then(() => deleteLoggedAuthorPostList(req.user))
			.then(() => deleteAuthorPostList(req.user))
			.then(() => Author.findByIdAndDelete(req.user._id))
			.then(() => generatePostListPages())
			.then(() => res.redirect(req.baseUrl))
			.catch((err) => next(err));
	},
];
