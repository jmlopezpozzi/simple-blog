/* Controller helpers */

"use strict";


const {param, validationResult} = require("express-validator");


exports.getPostPreviewObject = (post, minCharacters, extraCharacters,
                                moreString = "") =>
{
	let postText = post.text ? post.text : post.draftText;
	let extraIndex = postText.slice(minCharacters).match(/(.$)|[\s.,;:`'"]/);
	let totalChars = minCharacters +
	                 (extraIndex == null ? 0 : extraIndex.index);
	let previewText = extraIndex == null || extraIndex.index > extraCharacters ?
	                  postText.slice(0, minCharacters) :
	                  postText.slice(0, totalChars + (extraIndex[1] ? 1 : 0));  // If extraIndex[1] is defined we cut after minCharacters at the end of text, we add 1 so we don't lose the las letter

	let readMore = false;
	if (postText.length > minCharacters + extraCharacters) {
		previewText += moreString;
		readMore = true;
	}

	return {text: previewText, readMore};
}


function e404ForValidationFailureMiddleware(req, res, next) {
	let results = validationResult(req);
	if (!results.isEmpty()) {
		let err = new Error(req.app.get("env") === "development" ?
		                    JSON.stringify(results.array()) :
		                    "404 - Not found");
		err.status = 404;
		return next(err);
	}
	next();
}


// Returns a middleware chain that checks if the Id for a given field with a
// SchemaType of ObjectId referencing a document of model fieldModel is
// correct and corresponds to an existing document. Finds the document in the
// database (for the existence check) and saves it in res.locals[fieldName]
// for later use during the request.

exports.fieldCheckIdAndRetrieve = (fieldModel, fieldName) => {
	return [
		param("id", `Bad ${fieldName} Id`).isMongoId(),
		e404ForValidationFailureMiddleware,

		(req, res, next) => {
			fieldModel.findById(req.params.id)
				.then((fieldDocument) => {
					if (fieldDocument == null) {
						let err = new Error(
							req.app.get("env") === "development" ?
							`${fieldName} Id matches no ${fieldName}` :
							"404 - Not found"
						);
						err.status = 404;
						throw err;
					}
					res.locals[fieldName] = fieldDocument;
					next();
				})
				.catch((err) => next(err));
		},
	];
};


exports.assertLoggedUser = (req, res, next) => {
	if (!req.user) {
		res.redirect("/login");
		return;
	}
	next();
}


exports.sortPosts = (posts) => {
	if (!Array.isArray(posts)) {
		return;
	}
	posts.sort((a, b) => {
		let aa = a.sortingDate;
		let bb = b.sortingDate;
		return aa < bb ? 1 : (aa > bb ? -1 : 0);
	});
}
