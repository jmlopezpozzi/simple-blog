/* Posts controller */

"use strict";


const {body, param, validationResult} = require("express-validator");

const {config} = require("../config/config");

const Post = require("../models/post");
const Author = require("../models/author");

const {
	getPostPreviewObject,
	fieldCheckIdAndRetrieve,
	assertLoggedUser,
	sortPosts,
} = require("./controllerHelpers");

const {
	getPage,
	getPostListPage,
	getPostListNumPages,
	getPostsPerPage,
	generatePostListPages,
	generateAuthorPostListPages,
	generateLoggedAuthorPostListPages,
} = require("./postPagination");


const minPreviewCharacters = config.postList.previewLength;
const extraPreviewCharacters = config.postList.previewMargin;

const getPostPreview =
	config.postList.showPreview ?
	(post) => {
		post.preview = getPostPreviewObject(post, minPreviewCharacters,
		                                    extraPreviewCharacters, " ...");
	} :
	(post) => {};


exports.post_list = (req, res, next) => {
	let numPages = getPostListNumPages();
	let page = getPage(req.query.page, 1, numPages);  // NOTE: Page is 1-indexed
	if (!page) {
		let err = new Error(req.app.get("env") === "development" ?
		                    "Invalid page number" :
		                    "404 - Not found");
		err.status = 404;
		next(err);
		return;
	}

	Post.find({"_id": {$in: getPostListPage(page - 1)}})
		.limit(getPostsPerPage())
		.populate("author")
		.then((postList) => {
			sortPosts(postList);
			postList.forEach(getPostPreview);
			res.render("post_list", {
				title: "Post list",
				user: req.user,
				postList,
				page,
				numPages,
			});
		})
		.catch((err) => next(err));
}


exports.post_new = [
	assertLoggedUser,

	(req, res, next) => {
		res.render("post_form", {
			title: "New post by " + req.user.name,
			user: req.user,
		});
	},
]


const postDataValidateAndSanitize = [
	body("title", "Title must not be empty nor have more than 256 characters")
		.stripLow()
		.trim()
		.isLength({min: 1, max: 256}),
	body("text")
		.stripLow()
		.trim(),  // NOTE: There user provided values MUST be escaped in Pug (so the final HTML document does not contain code)
];


function getPostFormData(req) {
	return {
		author: req.user._id,
		title: req.body.title,
		draftText: req.body.draft_text,
	};
}


exports.post_new_post = [
	assertLoggedUser,
	postDataValidateAndSanitize,

	(req, res, next) => {
		let postFormData = getPostFormData(req);
		let post = new Post(postFormData);
		Object.assign(post, postFormData);
		if (formErrorsResponse(req, res, next, post)) {
			return;
		}
		let timestamp = Date.now();
		post.draftDate = timestamp;
		post.published = false;
		if (req.body.publish == "yes") {
			post.publicationDate = timestamp;
			post.text = post.draftText;
			post.published = true;
		}
		post.save()
			.then((post) => {
				return generateLoggedAuthorPostListPages(req.user)
					.then(() => post);
			})
			.then((post) => {
				if (req.body.publish == "yes") {
					return generatePostListPages()
						.then(() => generateAuthorPostListPages(req.user))
						.then(() => post);
				}
				return post;
			})
			.then((post) => res.redirect(post.url))
			.catch((err) => next(err));
	},
];


function assertUserIsAuthor(req, res, next) {
	let post = res.locals.post;
	let userId = req.user._id;
	if (userId.toString() != post.author.toString()) {
		res.redirect(post.url);
		return;
	}
	next();
}


exports.post_update = [
	assertLoggedUser,
	fieldCheckIdAndRetrieve(Post, "post"),
	assertUserIsAuthor,

	(req, res, next) => {
		let post = res.locals.post;
		res.render("post_form", {
			title: "Updating post " + post.title,
			user: req.user,
			post,
		});
	},
]


exports.post_update_post = [
	assertLoggedUser,
	fieldCheckIdAndRetrieve(Post, "post"),
	assertUserIsAuthor,
	postDataValidateAndSanitize,

	(req, res, next) => {
		let postFormData = getPostFormData(req);
		let post = res.locals.post;
		Object.assign(post, postFormData);
		if (formErrorsResponse(req, res, next, post)) {
			return;
		}
		let timestamp = Date.now();
		post.draftDate = timestamp;
		if (req.body.publish == "yes") {
			if (post.published) {
				post.lastUpdateDate = timestamp;
			}
			else {
				post.publicationDate = timestamp;
				post.published = true;
			}
			post.text = post.draftText;
		}
		post.save()
			.then((post) => {
				return generateLoggedAuthorPostListPages(req.user)
					.then(() => post);
			})
			.then((post) => {
				if (req.body.publish == "yes") {
					return generatePostListPages()
						.then(() => generateAuthorPostListPages(req.user))
						.then(() => post);
				}
				return post;
			})
			.then((post) => res.redirect(post.url))
			.catch((err) => next(err));
	},
];


// Checks for validation errors. Returns false if there are none. Returns true
// and re-renders the form with the previously entered values and the list of
// validation errors if said errors are found.

function formErrorsResponse(req, res, next, postValues) {
	let result = validationResult(req);
	if (result.isEmpty()) {
		return false;
	}

	let errors = result.array();
	let postTitle = req.params.id ?
					res.locals.post.title :
	                null;
	let title = postTitle ?
	            "Updating post " + postTitle :
	            "New post by " + req.user.name;
	res.render("post_form", {
		title,
		user: req.user,
		post: postValues,
		errors,
	});

	return true;
}


function postShowGeneric(deletePrompt = false) {
	return (req, res, next) => {
		let post = res.locals.post;
		if (!post.published &&
		    (!req.user || req.user._id.toString() != post.author.toString()))
		{
			let err = new Error("404 - Not found");
			err.status = 404;
			throw err;
		}

		post.populate("author")
			.execPopulate()
			.then((post) => {
				res.render("post_show", {
					title: post.title,
					user: req.user,
					post,
					deletePrompt,
				});
			})
			.catch((err) => next(err));
	};
}


exports.post_show = [
	fieldCheckIdAndRetrieve(Post, "post"),
	postShowGeneric(),
];


exports.post_delete = [
	assertLoggedUser,
	fieldCheckIdAndRetrieve(Post, "post"),
	assertUserIsAuthor,
	postShowGeneric(true),
];


exports.post_delete_post = [
	assertLoggedUser,
	fieldCheckIdAndRetrieve(Post, "post"),
	assertUserIsAuthor,

	(req, res, next) => {
		Post.findByIdAndDelete(req.body.postid)
			.then(() => generatePostListPages())
			.then(() => generateAuthorPostListPages(req.user))
			.then(() => generateLoggedAuthorPostListPages(req.user))
			.then(() => res.redirect(req.user.url))
			.catch((err) => next(err));
	},
]
