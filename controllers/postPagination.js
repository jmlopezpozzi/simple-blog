/* Post pagination */

"use strict";


const debug = require("debug")("simple-blog:postPagination");

const Post = require("../models/post");
const Author = require("../models/author");

const {config} = require("../config/config");

const {sortPosts} = require("./controllerHelpers");


exports.getPage = (page, min, max) => {
	if (!page) {
		return min;
	}
	let pageNumber = Number(page);
	if (Number.isNaN(pageNumber)) {
		return null;
	}
	if (pageNumber < min || pageNumber > max) {
		return null;
	}
	return page;
};


function arrayOfArrays(array, numItems) {
	let result = [];
	let iterations = Math.ceil(array.length / numItems);
	let start = 0;
	let end = numItems;
	for (let i = 0; i < iterations; i += 1) {
		result.push(array.slice(start, end));
		start += numItems;
		end += numItems;
	}
	return result;
}


function generatePostIndex() {
	return Post.find({"published": true}, "publicationDate lastUpdateDate")
		.then((postList) => {
			sortPosts(postList);
			return postList.map((post) => post._id);
		})
		.catch((err) => debug(err));
}


const postsPerPage = config.postList.postsPerPage;


exports.getPostsPerPage = () => {
	return postsPerPage;
}


let postListPageTable;


function generatePostListPages() {
	return generatePostIndex()
		.then((postIndex) => {
			postListPageTable = arrayOfArrays(postIndex, postsPerPage);
		});
};

exports.generatePostListPages = generatePostListPages;


exports.getPostListPage = (page) => {
	return postListPageTable[page];
};


exports.getPostListNumPages = () => {
	return postListPageTable.length;
};


function generateAuthorPostIndex(author) {
	return Post.find({"author": author, "published": true},
	                 "publicationDate lastUpdateDate")
		.then((postList) => {
			sortPosts(postList);
			return postList.map((post) => post._id);
		})
		.catch((err) => debug(err));
}


const authorPostsPerPage = config.authorShow.postsPerPage;


exports.getAuthorPostsPerPage = () => {
	return authorPostsPerPage;
}


let authorPostListPageTables = {};


function generateAuthorPostListPages(author) {
	return generateAuthorPostIndex(author)
		.then((postIndex) => {
			let authorTable = arrayOfArrays(postIndex, authorPostsPerPage);
			authorPostListPageTables[author._id] = authorTable;
		});
}

exports.generateAuthorPostListPages = generateAuthorPostListPages;


function fillAuthorPostListPageTables() {
	let tables = [];
	return Author.find({}, "_id")
		.then((authorList) => {
			for (let author of authorList) {
				tables.push(generateAuthorPostListPages(author));
			}
			return Promise.all(tables);
		});
};


exports.getAuthorPostListPage = (author, page) => {
	return authorPostListPageTables[author._id][page];
};


exports.getAuthorPostListNumPages = (author) => {
	return authorPostListPageTables[author._id].length;
};


exports.deleteAuthorPostList = (author) => {
	delete authorPostListPageTables[author._id];
};


let loggedAuthorPostListPageTables = {};


function generateLoggedAuthorPostIndex(author) {
	return Post.find({"author": author},
	                 "publicationDate lastUpdateDate draftDate")
		.then((postList) => {
			sortPosts(postList);
			return postList.map((post) => post._id);
		})
		.catch((err) => debug(err));
}


exports.generateLoggedAuthorPostListPages = (author) => {
	return generateLoggedAuthorPostIndex(author)
		.then((postIndex) => {
			let authorTable = arrayOfArrays(postIndex, authorPostsPerPage);
			loggedAuthorPostListPageTables[author._id] = authorTable;
		});
};


exports.getLoggedAuthorPostListPage = (author, page) => {
	return loggedAuthorPostListPageTables[author._id][page];
};


exports.getLoggedAuthorPostListNumPages = (author) => {
	return loggedAuthorPostListPageTables[author._id].length;
};


exports.deleteLoggedAuthorPostList = (author) => {
	delete loggedAuthorPostListPageTables[author._id];
};


generatePostListPages();
fillAuthorPostListPageTables();


// NOTE: NODE_ENV has to be explicitly set to "development" for this function to be exported
if (process.env.NODE_ENV === "development") {
	exports.postPaginationDebugInfo = () => {
		console.log("postPagination debug info:");
		console.log("postListPageTable:");
		console.log(postListPageTable);
		console.log("authorPostListPageTables:");
		console.log(authorPostListPageTables);
		console.log("loggedAuthorPostListPageTables");
		console.log(loggedAuthorPostListPageTables);
	};
}
