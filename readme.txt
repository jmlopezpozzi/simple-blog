SIMPLE-BLOG
by Juanmi López


DESCRIPTION

simple-blog is a web application that can be deployed in a compatible
server environment to have a website functioning as a blog.

It is built on Node.js using the express framework. It uses MongoDB for
storing data, the Pug HTML templating engine for generating markup and
the Sass CSS preprocessor for styling. The resulting user-facing
website does not need JavaScript to operate properly.

simple-blog supports author accounts from which posts can be written
and published.


USAGE

simple-blog needs a Node.js runtime and a MongoDB connection to
function, and npm (Node Package Manager) for installation and start up.

Two environment variables must be set for the application to start
running:

    MONGODB_URI, containing a valid MongoDB connection string URI.
    SESSION_SECRET, a string needed to generate session ids.

Othe environment variables that control the behavior of the application
are:

    NODE_ENV, when set to "production" starts express in production
      mode. This setting should be used for performance gains on final
      deployments.
    DEBUG, controls the logging of error and warning messages (using
      the "debug" npm package). All the internal application modules
      log these messages with the "simple-blog:" prefix, so setting
      "DEBUG=simple-blog:*" will make them all log.
    PORT, controls the HTTP port the application will be listening to
      (port number 3000 is used by default).
    BLOG_CONFIG, allows to configure application settings (more details
      below).

Run the command "npm install" once to fetch all the dependencies from
npm. Run "npm start" to start the application.

Some settings of simple-blog can be configured using a JSON file. A
default configuration file is read from "config/config.json". This
configuration can be selectively overriden by passing to the
application, as its first command line argument or via the environment
variable BLOG_CONFIG, either the path of a JSON file or a valid JSON
string containing the properties to override. Properties set using the
command line have priority over the ones set through the environment.

One of these configuration options is "allowRegistration", which can be
either true or false (the default). When set to true, the website will
show links to the "Register" and "Log in" pages. Authors can create and
access their accounts following these respective links. Authors have to
be registered to be able to log in and logged in to be able to post.

Starting the application with the configuration property
"allowRegistration" set to false makes the website not show the
aforementioned links and makes the "Register" page inaccessible. The
"Login" page however, can still be found at the route "/login" and
previously registered authors can use it to log into their accounts.

The rest of the configuration options are:
    "blogTitle": (string) The title of the blog.
    "dateFormat": (string) Format string that defines how dates are
      shown (uses the "moment" npm package).
  The sections "postList" and "authorShow" have the same set of
  options:
    "postsPerPage": (number) How many posts to show in each page of the
      list.
    "showPreview": (bool) Show (or not) the first words of the post.
    "previewLength": (number) Minimum number of characters to show in
      the post preview.
    "previewMargin": (number) Maximum number of characters the software
      is allowed to use after "previewLength" characters to try to find
      a place in the post text where it can cut without splitting a
      word, when generating the preview.

The website has a general post list where all blog posts are listed and
an individual post lists for each author. Logged in authors will find
the option to write a new post when in their own posts list. Posts can
be saved and optionally published. Unpublished posts will only be shown
to its author. Logged in authors will find the option to update or
delete a previouly written post or draft when in that post page.
Authors have the option of changing their author name when in their own
posts list. Finally, authors can also delete their accounts from the
same page. Doing so deletes all of the author's posts. Logged in
authors can log out by clicking on the "Log out" link usually found in
the site header (or navigating to the "/logout" route).

simple-blog uses the "helmet" npm package to set some HTTP headers in
the website in order to make it more secure. The configuration used is
a bit more strict than the one "helmet" uses by default. Of note is
that the "Content-Secure-Policy" header is set to block all third party
scripts. Users wanting to add third party scripts to their website will
habe to edit this configuration manually.


NOTES

simple-blog is mostly developed and tested using Node.js version
12.16.2, npm version 6.14.4, MongoDB version 6.3.6 under an x86_64
machine running Ubuntu 18.04.
