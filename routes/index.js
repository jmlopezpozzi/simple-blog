"use strict";


const express = require("express");
const router = express.Router();
const passport = require("passport");
const {body, validationResult} = require("express-validator");

const {config} = require("../config/config");

const Author = require("../models/author");

const {
	generateAuthorPostListPages,
	generateLoggedAuthorPostListPages,
	deleteLoggedAuthorPostList,
} = require("../controllers/postPagination");


// Root

router.get("/", (req, res, next) => {
	res.redirect("/posts");
});


// Registration

if (config.allowRegistration) {
	router.get("/register", (req, res, next) => {
		res.render("register", {
			title: "Register",
			user: req.user,
		});
	});

	router.post("/register",
		body("username")
			.isLength({min: 2, max: 32})
			.withMessage("Username must have more than 2 and less than 32 characters")
			.matches(/^[^\W]+$/)
			.withMessage("Username contains invalid characters"),

		body("password")
			.isLength({min: 6})
			.withMessage("Password must have at least 6 characters"),

		(req, res, next) => {
			let errors = validationResult(req);
			if (!errors.isEmpty()) {
				errors = errors.array();
				res.render("register", {
					title: "Register",
					user: req.user,
					name: req.body.name,
					username: req.body.username,
					errors,
				});
				return;
			}
			Author.register(new Author({
				name: req.body.name,
				username: req.body.username,
			}), req.body.password)
				.then(() => next())
				.catch((err) => {
					next(err);
				});
		},

		passport.authenticate("local", {
			failureRedirect: "/register",
		}),

		(req, res, next) => {
			generateAuthorPostListPages(req.user)
				.then(() => generateLoggedAuthorPostListPages(req.user))
				.then(() => res.redirect("/user"))
				.catch((err) => next(err));
		}
	);
}


// Log in and out

router.get("/login", (req, res, next) => {
	res.render("login", {
		title: "Log in",
		user: req.user,
	});
});


router.post("/login",
	passport.authenticate("local", {
		failureRedirect: "/login",
	}),

	(req, res, next) => {
		generateLoggedAuthorPostListPages(req.user)
			.then(() => res.redirect("/user"))
			.catch((err) => next(err));
	}
);


router.get("/logout", (req, res, next) => {
	if (!req.user) {
		res.redirect("/user");
		return;
	}
	deleteLoggedAuthorPostList(req.user);
	let exitingUser = req.user.username;
	req.logout();
	res.render("user", {exitingUser});
});


router.get("/user", (req, res, next) => res.render("user", {user: req.user}));



module.exports = router;
