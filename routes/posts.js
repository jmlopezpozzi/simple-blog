/* Posts router */

"use strict";


const express = require("express");
const router = express.Router();

const postController = require("../controllers/postController");


router.get("/", postController.post_list);
router.get("/new", postController.post_new);
router.post("/new", postController.post_new_post);
router.get("/:id/update", postController.post_update);
router.post("/:id/update", postController.post_update_post);
router.get("/:id/delete", postController.post_delete);
router.post("/:id/delete", postController.post_delete_post);
router.get("/:id", postController.post_show);


module.exports = router;
