/* Authors router */

"use strict";


const express = require("express");
const router = express.Router();

const authorController = require("../controllers/authorController");


router.get("/", authorController.author_list);
router.get("/update", authorController.author_update);
router.post("/update", authorController.author_update_post);
router.get("/delete", authorController.author_delete);
router.post("/delete", authorController.author_delete_post);
router.get("/:id", authorController.author_show);


module.exports = router;
