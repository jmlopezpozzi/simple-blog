const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const moment = require("moment");

const {dateFormat} = require("../config/config").config;


let postSchema = new Schema({
	author: {
		type: Schema.Types.ObjectId,
		ref: "Author",
		required: true,
	},
	lastUpdateDate: Date,
	publicationDate: Date,
	draftDate: {
		type: Date,
		required: true,
	},
	title: {
		type: String,
		required: true,
		minlength: 1,
		maxlength: 256,
	},
	text: String,
	draftText: String,
	published: {
		type: Boolean,
		required: true,
	},
});

postSchema.virtual("sortingDate").get(function () {
	return this.lastUpdateDate ?
	       this.lastUpdateDate :
	       (this.publicationDate ? this.publicationDate : this.draftDate);
});

postSchema.virtual("formattedLastUpdateDate").get(function () {
	return moment(this.lastUpdateDate).format(dateFormat);
});

postSchema.virtual("formattedPublicationDate").get(function () {
	return moment(this.publicationDate).format(dateFormat);
});

postSchema.virtual("formattedDraftDate").get(function () {
	return moment(this.draftDate).format(dateFormat);
});

postSchema.virtual("url").get(function () {
	return "/posts/" + this._id;
});


module.exports = mongoose.model("Post", postSchema);
