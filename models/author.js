const mongoose = require("mongoose");
const passportLocalMongoose = require("passport-local-mongoose");
const Schema = mongoose.Schema;
const Post = require("./post");


let authorSchema = new Schema({
	name: {
		type: String,
		required: true,
		minlength: 1,
		maxlength: 128,
	},
	username: {
		type: String,
		required: true,
		minlength: 2,
		maxlength: 32,
	},
});

authorSchema.virtual("posts").get(function () {
	return Post.find({author: this._id});
});

authorSchema.virtual("url").get(function () {
	return "/authors/" + this._id;
});


// passport-local-mongoose plugin
// NOTE: Uses "username" field
// NOTE: Previous "DeprecationWarning: collection.ensureIndex is deprecated. Use createIndexes instead."
// message disappears after having defined the "username" field in authorSchema

authorSchema.plugin(passportLocalMongoose, {passwordValidator});

function passwordValidator(password, callback) {
	if (typeof(password) !== "string" || password.length < 6) {
		callback(new Error("Password is too short"));
	}
	callback();
}


module.exports = mongoose.model("Author", authorSchema);
